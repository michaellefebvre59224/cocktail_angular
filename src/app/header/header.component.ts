import {Component, Input, OnInit, Output} from '@angular/core';
import {Panier} from '../Panier';
import {Cocktail} from '../Cocktail';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  @Input() panierClient: Panier;

  setPanierClient(panierClient): void{
    this.panierClient = panierClient;
  }

  ngOnInit(): void {
  }

}
