import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {COCKTAILS} from '../init_cocktails';
import {Cocktail} from '../Cocktail';
import {Panier} from '../Panier';
import {applySourceSpanToExpressionIfNeeded} from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-list-cocktail',
  templateUrl: './list-cocktail.component.html',
  styleUrls: ['./list-cocktail.component.css']
})
export class ListCocktailComponent implements OnInit {

  id ;
  newCocktail = new Cocktail();
  modifCocktail = new Cocktail();

  cocktails = COCKTAILS;
  selectedCoctail: Cocktail;
  panierClient: Panier;

  @Output() showIngredient = new EventEmitter<any>();

  @Output() envoiePanier = new EventEmitter<any>();

  constructor() { }

  public ajoutPanier(event, cocktail): void{
    console.log(cocktail);
    this.panierClient.cocktails.push(cocktail);
    console.log(this.panierClient.getPrix());
    console.log(cocktail.prix);
    this.panierClient.setPrix(this.panierClient.getPrix() + cocktail.prix);
    console.log(this.panierClient.getPrix());
    this.envoiePanier.emit(this.panierClient);
  }

  public updateCocktailNom(event): void{
    console.log(event);
    this.modifCocktail.nom = event.target.value;
  }

  public updateCocktailPrix(event): void{
    console.log(event.target.value);
    this.modifCocktail.prix = event.target.value;
  }

  public setSelectedCocktail(event, cocktail): void{
    if (this.modifCocktail.id !== cocktail.id){
      console.log('modification de modif cocktail');
      this.modifCocktail = cocktail;
    }
    this.selectedCoctail = cocktail;
    this.showIngredient.emit(this.selectedCoctail);
  }

  public ajoutCocktail(event): void{
    this.id = this.cocktails.length + 1;
    /*this.cocktails.forEach(cocktail => {
      this.id = cocktail.id + 1;
    });*/
    this.newCocktail.id = this.id;
    this.cocktails.push(this.newCocktail);
    this.newCocktail = new Cocktail();
  }

  public modifierCocktail(event, modifCoc): void{
    console.log(this.selectedCoctail);
    console.log(modifCoc.nom);
    console.log(modifCoc.prix);
    this.cocktails.forEach(c => {
      if (c.id === this.selectedCoctail.id) {
        c.prix = modifCoc.prix;
        c.nom = modifCoc.nom;
      }
    });
  }

  // tslint:disable-next-line:typedef
  public suppressionCocktail(event, cocktail){
    // tslint:disable-next-line:prefer-const
    let idCock: number;
    let i = 0;
    console.log(cocktail);
    this.cocktails.forEach(c => {
      if (c.id === cocktail.id){
        console.log(i);
        this.cocktails.splice(i, 1);
      }
      i++;
    });
  }

  ngOnInit(): void {
    this.selectedCoctail = new Cocktail();

    this.panierClient = new Panier();
    this.panierClient.setPrix(0);

  }

}
