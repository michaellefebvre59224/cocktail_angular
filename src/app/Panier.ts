import {Cocktail} from './Cocktail';

export class Panier {

  private cocktails: Cocktail[];
  private prix: number;

  constructor(prix?: number) {
    this.prix = prix;
    this.cocktails = [];
  }


  getCocktails(): Cocktail[] {
    return this.cocktails;
  }

  setCocktails(value: Cocktail[]): void {
    this.cocktails = value;
  }

  getPrix(): number {
    return this.prix;
  }

  setPrix(value: number): void {
    this.prix = value;
  }

}
