export class Ingredient {
  nom: string;
  quantite: string;

  constructor(nom?: string, quantite?: string) {
    this.nom = nom;
    this.quantite = quantite;
  }

}
