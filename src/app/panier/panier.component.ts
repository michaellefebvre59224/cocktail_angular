import {Component, Input, OnInit} from '@angular/core';
import {Panier} from '../Panier';
import {Cocktail} from '../Cocktail';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {

  constructor() { }

  @Input() panierClient: Panier;

  ngOnInit(): void {
    this.panierClient = new Panier();
  }

}
