import { Ingredient } from './Ingredient';

export class Cocktail {
  public id: number;
  public nom: string;
  public prix: number;
  public ingredients: Ingredient[];

  constructor(id?: number, nom?: string, prix?: number) {
    this.id = id;
    this.nom = nom;
    this.prix = prix;
    this.ingredients = [];
  }


}
