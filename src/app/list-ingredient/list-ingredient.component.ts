import {Component, Input, OnInit} from '@angular/core';
import {Cocktail} from '../Cocktail';
import {Ingredient} from '../Ingredient';


@Component({
  selector: 'app-list-ingredient',
  templateUrl: './list-ingredient.component.html',
  styleUrls: ['./list-ingredient.component.css']
})
export class ListIngredientComponent implements OnInit {

  constructor() { }

  selectedIngredient: Ingredient;
  modifIngredient = new Ingredient();

  newIngredient = new Ingredient();


  @Input() cocktail: Cocktail;

  public ajoutIngredient(event): void{
    this.cocktail.ingredients.push(this.newIngredient);
    this.newIngredient = new Ingredient();
  }

  // tslint:disable-next-line:typedef
  public suppressionIngredient(event, ingredient){
    // tslint:disable-next-line:prefer-const
    let idCock: number;
    let i = 0;
    this.cocktail.ingredients.forEach(c => {
      if (c.nom === ingredient.nom){
        console.log(i);
        this.cocktail.ingredients.splice(i, 1);
      }
      i++;
    });
  }

  public setSelectedIngredient(event, ingredient): void{
    if (this.modifIngredient.nom !== ingredient.nom){
      console.log('modification de modif cocktail');
      this.modifIngredient = ingredient;
    }
    this.selectedIngredient = ingredient;
  }

  public updateIngredientNom(event): void{
    console.log(event);
    this.modifIngredient.nom = event.target.value;
  }

  public updateIngredientQuantite(event): void{
    console.log(event.target.value);
    this.modifIngredient.quantite = event.target.value;
  }


  public modifierIngredient(event, modifCoc): void{
    console.log(this.selectedIngredient);
    this.cocktail.ingredients.forEach(c => {
      if (c.nom === this.selectedIngredient.nom) {
        c.quantite = modifCoc.quantite;
        c.nom = modifCoc.nom;
      }
    });
  }

  ngOnInit(): void {
    this.selectedIngredient = new Ingredient();
  }

}
