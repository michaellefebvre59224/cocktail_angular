import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListCocktailComponent } from './list-cocktail/list-cocktail.component';
import { ListIngredientComponent } from './list-ingredient/list-ingredient.component';
import {FormsModule} from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { PanierComponent } from './panier/panier.component';

@NgModule({
  declarations: [
    AppComponent,
    ListCocktailComponent,
    ListIngredientComponent,
    HeaderComponent,
    PanierComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
