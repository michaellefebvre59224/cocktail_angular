import { Component, Input } from '@angular/core';
import {Cocktail} from './Cocktail';
import {Panier} from './Panier';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'cocktails2';

  selectedCocktail: Cocktail;
  panierClient: Panier;

  constructor() {
  }

  setPanierClient(panierClient): void{
    this.panierClient = panierClient;
  }

  setDetailIngredient(cocktail): void{
    this.selectedCocktail = cocktail;
  }
}
